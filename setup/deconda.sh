# conda environments mess with the atlas setup
if command -v conda &> /dev/null && [[ -n "$CONDA_DEFAULT_ENV" ]]; then
    echo "=== deactivating conda ==="
    conda deactivate
fi
