#
# Cmake for FTagDumper
#

# Set the name of the package:
atlas_subdir( FTagDumper )

# External(s) used by the package:
find_package(HDF5 1.10.1 REQUIRED COMPONENTS CXX C)
find_package(nlohmann_json REQUIRED)
find_package(Boost REQUIRED COMPONENTS program_options)

# We don't want any warnings in compilation
add_compile_options(-Werror)

# We have to define the libraries first because we might not use
# some. This lets us add them condionally.
set(LINK_LIBRARIES
  xAODRootAccess
  xAODCaloEvent
  xAODTracking
  xAODPFlow
  xAODJet
  xAODTruth
  xAODMuon
  xAODEgamma
  HDF5Utils
  xAODCutFlow
  AsgTools
  JetCalibToolsLib
  JetSelectorToolsLib
  JetUncertaintiesLib
  JetCPInterfaces
  InDetTrackSelectionToolLib
  FlavorTagDiscriminants
  PathResolver
  CutBookkeeperUtils
  JetMomentToolsLib
  JetJvtEfficiencyLib
  InDetTrackSystematicsToolsLib
  xAODHIEvent
  MCTruthClassifierLib
  JetWriters
  ElectronPhotonSelectorToolsLib
  )
# anything that is built conditionally goes here
if (NOT XAOD_STANDALONE)
  list(APPEND LINK_LIBRARIES TrigDecisionToolLib)
endif()

# common requirements
atlas_add_library(dataset-dumper
  src/BTagJetWriter.cxx
  src/BTagJetWriterUtils.cxx
  src/BTagJetWriterConfig.cxx
  src/VariablesByType.cxx
  src/SubjetWriter.cxx
  src/SubstructureAccessors.cxx
  src/HitWriter.cxx
  src/TruthWriter.cxx
  src/TrackSelector.cxx
  src/TruthTools.cxx
  src/ConstituentSelector.cxx
  src/DecoratorExample.cxx
  src/JetTruthAssociator.cxx
  src/JetTruthMerger.cxx
  src/TrackVertexDecorator.cxx
  src/ConfigFileTools.cxx
  src/BTagInputChecker.cxx
  src/JobMetadata.cxx
  src/errorLogger.cxx
  src/streamers.cxx
  src/trackSort.cxx
  src/cleanHits.cxx
  src/processEvent.cxx
  src/HitDecorator.cxx
  src/BJetShallowCopier.cxx
  src/JetDumperConfig.cxx
  src/JetDumperTools.cxx
  src/JetLeptonDecayLabelDecorator.cxx
  src/LeptonTruthDecorator.cxx
  src/JetTruthSummaryDecorator.cxx
  src/SoftElectronSelector.cxx
  src/TrackSubjetsDecorator.cxx
  src/TriggerVRJetOverlapDecoratorTool.cxx
  src/HitdRMinDecoratorAlg.cxx
  src/HitTruthDecoratorAlg.cxx

  PUBLIC_HEADERS src
  LINK_LIBRARIES ${LINK_LIBRARIES} ${Boost_LIBRARIES}
  )

# Build the executables
atlas_add_executable( test-config-merge
  util/test-config-merge.cxx
  src/ConfigFileTools.cxx
  INCLUDE_DIRS
  ${Boost_INCLUDE_DIRS}
  LINK_LIBRARIES
  ${Boost_LIBRARIES}
  -lstdc++fs nlohmann_json::nlohmann_json
  )
atlas_add_executable( test-config-parse-single-b
  util/test-config-parse-single-b.cxx
  LINK_LIBRARIES dataset-dumper)

# we only build the algorithm stuff for Gaudi builds
if (NOT XAOD_STANDALONE)
  atlas_add_component(FTagDumper
    src/JetDumperAlg.cxx
    src/TriggerJetGetterAlg.cxx
    src/TrackSystematicsAlg.cxx
    src/TriggerBTagMatcherAlg.cxx
    src/JetMatcherAlg.cxx
    src/TruthJetPrimaryVertexDecoratorAlg.cxx
    src/JetSystematicsAlg.cxx
    src/MCTCDecoratorAlg.cxx
    src/HitELDecoratorAlg.cxx
    src/ParentLinkDecoratorAlg.cxx
    src/TrackFlowOverlapRemovalAlg.cxx
    src/FlowSelectorAlg.cxx
    src/GhostAssociationSystAlg.cxx
    src/components/*.cxx
    LINK_LIBRARIES
    dataset-dumper
    AnaAlgorithmLib
    GaudiKernel
    DerivationFrameworkInterfaces
    AthLinks
    AthenaBaseComps
    IH5GroupSvc
    )

  atlas_install_scripts(
    bin/*
    POST_BUILD_CMD ${ATLAS_FLAKE8} --extend-ignore=ATL902
    )

  atlas_install_python_modules(
    python/*.py
    python/blocks
    POST_BUILD_CMD ${ATLAS_FLAKE8} --extend-ignore=ATL902
    )
endif()


atlas_install_scripts(
  test/test-dumper
  test/test-configs-single-b
  test/test-output
  batch/batch-single-btag
  grid/grid-submit
  grid/get-submit-files
  )
