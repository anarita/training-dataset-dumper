#include "HitELDecoratorAlg.h"

#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "AthLinks/ElementLink.h"
#include "xAODTracking/TrackParticle.h"

HitELDecoratorAlg::HitELDecoratorAlg(
    const std::string &name, ISvcLocator *loc) : AthReentrantAlgorithm(name, loc),
                                                 m_HitCont("HitContainedInTrack"),
                                                 m_HitLink("HitToTrackLinks"),
                                                 m_TrackID("TrackID")
{
}

StatusCode HitELDecoratorAlg::initialize()
{
  ATH_MSG_DEBUG("Inizializing " << name() << "... ");
  ATH_CHECK(m_tracks.initialize());
  ATH_CHECK(m_JetPixelCluster.initialize());
  ATH_CHECK(m_JetSCTCluster.initialize());

  ATH_CHECK(m_track_ID.initialize());
  ATH_CHECK(m_JetSCTClusterTrackID.initialize());
  ATH_CHECK(m_JetPixelClusterTrackID.initialize());

  return StatusCode::SUCCESS;
}

StatusCode HitELDecoratorAlg::execute(const EventContext &ctx) const
{
  ATH_MSG_DEBUG("Executing " << name() << "... ");

  SG::ReadHandle<xAOD::TrackParticleContainer> tracks{m_tracks, ctx};
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> JetPixelCluster{m_JetPixelCluster, ctx};
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> JetSCTCluster{m_JetSCTCluster, ctx};

  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, int> JetPixelClusterTrackID{m_JetPixelClusterTrackID, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, int> JetSCTClusterTrackID{m_JetSCTClusterTrackID, ctx};
  SG::WriteDecorHandle<xAOD::TrackParticleContainer, int> track_ID{m_track_ID, ctx};

  if (!JetPixelCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find Pixel");
    return StatusCode::FAILURE;
  }
  if (!JetSCTCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find SCT");
    return StatusCode::FAILURE;
  }
  if (!tracks.isValid())
  {
    ATH_MSG_ERROR("Couldn't find tracks");
    return StatusCode::FAILURE;
  }

  // give each hit a unique ID
  int idx = 1; // starting at 1, hits not contained in tracks, will get 0
  for (const auto trk : *tracks)
  {
    track_ID(*trk) = idx;
    idx++; // starting at 1, hits not contained in tracks, will get 0
  }

  // loop over Hits and associate each hit, that was contained in a track reco, with the ID of the corresponding track
  std::vector<std::pair<SG::ReadHandle<xAOD::TrackMeasurementValidationContainer>, SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer,  int>>> pairVec;
  pairVec.emplace_back(std::move(JetPixelCluster), std::move(JetPixelClusterTrackID));
  pairVec.emplace_back(std::move(JetSCTCluster), std::move(JetSCTClusterTrackID));
  for (auto iter : pairVec)
  {
    for (const auto hit : *(iter.first))
    {

      int cont = m_HitCont(*hit);
      if (cont == 1)
      {
        std::vector<ElementLink<xAOD::TrackParticleContainer>> links = m_HitLink(*hit);
        const xAOD::TrackParticle *track_hit = **std::max_element(links.begin(), links.end(), [](auto &l1, auto &l2)
                                                                    { return (*l1)->pt() < (*l2)->pt(); });
        iter.second(*hit) = m_TrackID(*track_hit);
      }
      else
      {
        iter.second(*hit) = 0;
      }
    }
  }

  return StatusCode::SUCCESS;
}
