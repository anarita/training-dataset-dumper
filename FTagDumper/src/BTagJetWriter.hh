#ifndef BTAG_JET_WRITER_HH
#define BTAG_JET_WRITER_HH


namespace H5 {
  class Group;
}
namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

class BTagJetWriterConfig;
class IJetOutputWriter;
class JetOutputs;

#include <string>
#include <vector>
#include <map>
#include <memory>

namespace H5Utils {
  template<size_t N, typename T> class Writer;
}


class BTagJetWriter
{
public:
  BTagJetWriter(
    H5::Group& output_file,
    const BTagJetWriterConfig& jet);
  ~BTagJetWriter();
  BTagJetWriter(BTagJetWriter&&);
  BTagJetWriter(BTagJetWriter&) = delete;
  BTagJetWriter operator=(BTagJetWriter&) = delete;
  void write(const std::vector<const xAOD::Jet*> jets,
             const xAOD::EventInfo* = 0);
private:
  std::unique_ptr<IJetOutputWriter> m_hdf5_jet_writer;
  std::unique_ptr<H5Utils::Writer<0, const JetOutputs&>> m_event_writer;
};


#endif
