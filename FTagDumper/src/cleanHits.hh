#ifndef CLEANHITS_H
#define CLEANHITS_H

#include "JetDumperTools.hh"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

namespace xAOD {
  class TrackMeasurementValidation_v1;
  using TrackMeasurementValidation = TrackMeasurementValidation_v1;
  using TrackMeasurementValidationContainer =
    DataVector<TrackMeasurementValidation_v1>;
}

bool isGoodHit(const xAOD::TrackMeasurementValidation*);

std::vector<const xAOD::TrackMeasurementValidation*> cleanHits(const xAOD::TrackMeasurementValidationContainer*, bool);

template <typename T>
std::vector<const xAOD::TrackMeasurementValidation*> getHits(T& event, const HitConfig &jobcfgHits, const JetDumperTools& tools){
  const xAOD::TrackMeasurementValidationContainer* container_pix_hits = nullptr;
  //Retrieve the PixelClusters container from the event
  if( !event.retrieve(container_pix_hits, jobcfgHits.container_name).isSuccess() ) throw std::runtime_error("bad return code");
  //Mark all PixelClusters as not being SCT clusters
  for (const auto hit : *container_pix_hits) tools.dec.isSCT(*hit) = 0;
  //Clean the PixelClusters container if in jobconfig save_only_clean_hits is true
  auto pix_hits_cleaned = cleanHits(container_pix_hits, jobcfgHits.writer.save_only_clean_hits);
  //if in jobconfig save_sct is true
  if (jobcfgHits.writer.save_sct){
    const xAOD::TrackMeasurementValidationContainer* container_sct_hits = nullptr;
    std::vector<const xAOD::TrackMeasurementValidation*> pix_sct_hits;
    //Retrieve the SCTClusters container from the event
    if( !event.retrieve(container_sct_hits, "JetAssociatedSCTClusters").isSuccess() ) throw std::runtime_error("bad return code");
    //Mark all SCTClusters as being SCT clusters
    for (const auto hit : *container_sct_hits) tools.dec.isSCT(*hit) = 1;
    //use cleanHits to save sct hits from container to std::vector
    auto sct_hits = cleanHits(container_sct_hits, false);
    //marge the hits container
    size_t size = pix_hits_cleaned.size() + sct_hits.size();
    pix_sct_hits.reserve( size ); // preallocate memory
    pix_sct_hits.insert( pix_sct_hits.end(), pix_hits_cleaned.begin(), pix_hits_cleaned.end() ); // copy pix_hits_cleaned in pix_sct_hits
    pix_sct_hits.insert( pix_sct_hits.end(), sct_hits.begin(), sct_hits.end() ); //copy sct_hits in pix_sct_hits
    return pix_sct_hits;
  }else return pix_hits_cleaned;
}

#endif
