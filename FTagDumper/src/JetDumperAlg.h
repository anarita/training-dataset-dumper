#ifndef SINGLE_BTAG_ALG_HH
#define SINGLE_BTAG_ALG_HH

#include "HDF5Utils/IH5GroupSvc.h"

#include "xAODBTagging/BTaggingContainer.h"
#include "xAODJet/JetContainer.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"
#include "GaudiKernel/ServiceHandle.h"

#include <memory>

class JetDumperTools;
class JetDumperConfig;
class JetDumperOutputs;
namespace H5 {
  class H5File;
}

class JetDumperAlg: public AthAlgorithm
{
public:
  JetDumperAlg(const std::string& name, ISvcLocator* pSvcLocator);
  ~JetDumperAlg();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  std::string m_config_json;
  std::string m_metadata_file_name;
  Gaudi::Property<bool> m_force_full_precision {
    this, "forceFullPrecision", false, "force writing full precision"};

  std::unique_ptr<JetDumperConfig> m_config;

  std::unique_ptr<JetDumperTools> m_tools;
  std::unique_ptr<JetDumperOutputs> m_outputs;

  // stuff for data dependencies
  SG::ReadHandleKey<xAOD::JetContainer> m_jetKey{this, "jetKey", "", ""};
  std::vector<std::unique_ptr<SG::ReadDecorHandleKey<xAOD::BTagging>>> m_bDec;
  ServiceHandle<IH5GroupSvc> m_output_svc {
    this, "output", "", "output file service"};
  Gaudi::Property<std::string> m_h5_dir {
    this, "group", "", "Group in H5 file"};
};

#endif // SINGLE_BTAG_ALG_HH
